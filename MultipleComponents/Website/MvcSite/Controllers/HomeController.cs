﻿namespace MvcSite.Controllers
{
    using System.Security.Claims;
    using System.Web;
    using System.Web.Mvc;
    using Thinktecture.IdentityModel.Mvc;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Authenticated()
        {
            return View((User as ClaimsPrincipal).Claims);
        }

        [ResourceAuthorize("Read", "ContactDetails")]
        public ActionResult Authorize()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut();
            return View();
        }
    }
}