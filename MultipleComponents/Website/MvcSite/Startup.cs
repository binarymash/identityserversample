﻿namespace MvcSite
{
    using System.Collections.Generic;
    using System.IdentityModel.Tokens;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web.Helpers;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.Notifications;
    using Microsoft.Owin.Security.OpenIdConnect;
    using Owin;
    using Thinktecture.IdentityServer.Core;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AntiForgeryConfig.UniqueClaimTypeIdentifier = Constants.ClaimTypes.Subject;
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            //client config
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies"
            });

            app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
            {
                Authority = "http://localhost:44333",

                ClientId = "mvc",
                Scope = "openid profile roles",
                RedirectUri = "https://localhost:44301",
                ResponseType = "id_token",

                SignInAsAuthenticationType = "Cookies",

                UseTokenLifetime = false,

                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = OnSecurityTokenValidated
                }

            });

            app.UseResourceAuthorization(new AuthorizationManager());
        }

        private async Task OnSecurityTokenValidated(
            SecurityTokenValidatedNotification
                <Microsoft.IdentityModel.Protocols.OpenIdConnectMessage, OpenIdConnectAuthenticationOptions>
                notification)
        {
            var id = notification.AuthenticationTicket.Identity;

            // we want to keep first name, last name, subject and roles
            var givenName = id.FindFirst(Constants.ClaimTypes.GivenName);
            var familyName = id.FindFirst(Constants.ClaimTypes.FamilyName);
            var sub = id.FindFirst(Constants.ClaimTypes.Subject);
            var roles = id.FindAll(Constants.ClaimTypes.Role);
            var name = id.FindFirst(Constants.ClaimTypes.Name);
            // create new identity and set name and role claim type
            var nid = new ClaimsIdentity(
                id.AuthenticationType,
                Constants.ClaimTypes.GivenName,
                Constants.ClaimTypes.Role);

            if (givenName != null)
            {
                nid.AddClaim(givenName);
            }
            if (familyName != null)
            {
                nid.AddClaim(familyName);
            }
            if (name != null)
            {
                nid.AddClaim(name);
            }
            nid.AddClaim(sub);
            nid.AddClaims(roles);

            // add some other app specific claim
            nid.AddClaim(new Claim("app_specific", "some data"));

            notification.AuthenticationTicket = new AuthenticationTicket(
                nid,
                notification.AuthenticationTicket.Properties);
        }
    }
}