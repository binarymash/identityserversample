﻿namespace ConsoleServer.Stores
{
    using System.Collections.Generic;
    using Thinktecture.IdentityServer.Core.Models;

    static class Scopes
    {
        public static List<Scope> Get()
        {
            var scopes = new List<Scope>
            {
                new Scope
                {
                    Name = "PermissionToDoABC"
                },
                new Scope
                {
                    Name = "PermissionToDoDEF"
                },
                new Scope
                {
                    Enabled = true,
                    Name = "roles",
                    Type = ScopeType.Identity,
                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim("role")
                    }
                }
            };

            scopes.AddRange(StandardScopes.All);

            return scopes;
        }
    }
}
