﻿namespace IisWebApi
{
    using System.Web.Http;
    using Owin;
    using Thinktecture.IdentityServer.v3.AccessTokenValidation;

    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // configure web api
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            ConfigureAuthentication(app, config);

            app.UseWebApi(config);
        }

        public void ConfigureAuthentication(IAppBuilder app, HttpConfiguration config)
        {
            // require authentication for all controllers
            config.Filters.Add(new AuthorizeAttribute());

            // accept access tokens from identityserver and require a scope of 'api1'
            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = "http://localhost:44333",
                RequiredScopes = new[] { "PermissionToDoABC" }
            });
            
        }
    }
}