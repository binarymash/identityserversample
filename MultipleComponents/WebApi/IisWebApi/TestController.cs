﻿namespace IisWebApi
{
    using System.Linq;
    using System.Security.Claims;
    using System.Web.Http;

    [Route("test")]
    public class TestController : ApiController
    {
        public IHttpActionResult Get()
        {
            var caller = User as ClaimsPrincipal;

            return Json(new
            {
                message = "hello, here's your data",
                client = caller
            });

            //var subjectClaim = caller.FindFirst("sub");
            //if (subjectClaim != null)
            //{
            //    //human user
            //    return Json(new
            //    {
            //        message = "OK user",
            //        client = caller.FindFirst("client_id").Value,
            //        subject = subjectClaim.Value
            //    });

            //}

            ////machine user
            //return Json(new
            //{
            //    message = "OK computer",
            //    client = caller.FindFirst("client_id").Value
            //});
        }
    }
}