﻿namespace ConsoleServer.Stores
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using Thinktecture.IdentityServer.Core;
    using Thinktecture.IdentityServer.Core.Services.InMemory;

    static class Users
    {
        public static List<InMemoryUser> Get()
        {
            return new List<InMemoryUser>
            {
                new InMemoryUser
                {
                    Username = "bob", //authentication
                    Password = "secret", //authentication
                    Subject = "1", //unique ID
                    Claims = new[]
                    {
                        new Claim(Constants.ClaimTypes.GivenName, "Bob"),
                        new Claim(Constants.ClaimTypes.FamilyName, "Smith"),
                        new Claim(Constants.ClaimTypes.Role, "Geek"),
                        new Claim(Constants.ClaimTypes.Role, "Foo"),
                        new Claim(Constants.ClaimTypes.BirthDate, "1975-01-02")
                    }
                },
                new InMemoryUser
                {
                    Username = "alice",
                    Password = "secret",
                    Subject = "2",
                    Claims = new[]
                    {
                        new Claim(Constants.ClaimTypes.GivenName, "Alice"),
                        new Claim(Constants.ClaimTypes.FamilyName, "Jones"),
                        new Claim(Constants.ClaimTypes.Role, "Bar")
                    }                }
            };
        }
    }
}
