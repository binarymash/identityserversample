﻿namespace IdentityServerDemo
{
    using System.Collections.Generic;
    using Thinktecture.IdentityServer.Core.Services.InMemory;

    static class Users
    {
        public static List<InMemoryUser> Get()
        {
            return new List<InMemoryUser>
            {
                new InMemoryUser
                {
                    Username = "bob", //authentication
                    Password = "secret", //authentication
                    Subject = "1" //unique id
                },
                new InMemoryUser
                {
                    Username = "alice",
                    Password = "secret",
                    Subject = "2"
                }
            };
        }
    }
}
