﻿namespace MyConsoleApp1
{
    using System;
    using System.Net.Http;
    using Thinktecture.IdentityModel.Client;

    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Getting access token from identity server for the client MyConsoleApp1...");
            var accessToken = GetToken();
            System.Console.WriteLine(accessToken);

            System.Console.WriteLine("Calling API with client MyConsoleApp1's access token...");
            CallApi(accessToken);

            System.Console.WriteLine("Getting access token from identity server for the resource owner bob...");
            accessToken = GetUserToken();
            System.Console.WriteLine(accessToken);

            System.Console.WriteLine("Calling API with resource owner bob's access token...");
            CallApi(accessToken);

            System.Console.WriteLine("Hit return to finish");
            System.Console.ReadLine();
        }


        /// <summary>
        /// Requests the access token from the identity server using the console app's credentials
        /// </summary>
        /// <returns></returns>
        static TokenResponse GetToken()
        {
            var client = new OAuth2Client(
                new Uri("http://localhost:44333/connect/token"),
                "silicon",
                "F621F470-9731-4A25-80EF-67A6F7C5F4B8");

            var tokenResponse = client.RequestClientCredentialsAsync("api1").Result;

            return tokenResponse;
        }

        /// <summary>
        /// Requests the access token from the identity server for a resource owner using bob's credentials
        /// </summary>
        /// <returns></returns>
        static TokenResponse GetUserToken()
        {
            var client = new OAuth2Client(
                new Uri("http://localhost:44333/connect/token"),
                "carbon",
                "21B5F798-BE55-42BC-8AA8-0025B903DC3B");

            return client.RequestResourceOwnerPasswordAsync("bob", "secret", "api1").Result;
        }

        /// <summary>
        /// Calls the API using the access token
        /// </summary>
        /// <param name="response">The response.</param>
        static void CallApi(TokenResponse response)
        {
            var client = new HttpClient();
            client.SetBearerToken(response.AccessToken);

            try
            {
                var result = client.GetStringAsync("http://localhost:14869/test").Result;
                System.Console.WriteLine(result);
            }
            catch (AggregateException exs)
            {
                foreach (var ex in exs.InnerExceptions)
                {
                    var exception = ex as HttpRequestException;
                    if (exception != null)
                    {
                        //do something
                    }
                }
            }

            return;
        }

    }

}
