﻿namespace IdentityServerDemo
{
    using System;
    using Microsoft.Owin.Hosting;
    using Thinktecture.IdentityServer.Core.Logging;

    class Program
    {
        static void Main(string[] args)
        {
            LogProvider.SetCurrentLogProvider(new DiagnosticsTraceLogProvider());

            using (WebApp.Start<Startup>("http://localhost:44333"))
            {
                Console.WriteLine("server running... hit return to stop.");
                Console.ReadLine();
            }
        }
    }
}
