﻿namespace ConsoleServer
{
    using System;
    using System.IO;
    using System.Security.Cryptography.X509Certificates;
    using Microsoft.Owin.Security.Facebook;
    using Microsoft.Owin.Security.Google;
    using Owin;
    using Stores;
    using Thinktecture.IdentityServer.Core.Configuration;

    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var factory = InMemoryFactory.Create(
                //Mandatory
                scopes: Scopes.Get(),
                clients: Clients.Get(),
                users: Users.Get());

            //Mandatory for production

            //AuthorizationCodeStore: Implements storage and retrieval of authorization codes 
            //TokenHandleStore: Implements storage and retrieval of handles for reference tokens 
            //RefreshTokenStore: Implements storage and retrieval of refresh tokens 
            //ConsentStore: Implements storage and retrieval of consent decisions 
            //ViewService: Implements retrieval of UI assets. Defaults to using the embedded assets. 

            var options = new IdentityServerOptions
            {
                RequireSsl = false,
                Factory = factory,
                SigningCertificate = LoadCertificate(),

                AuthenticationOptions = new Thinktecture.IdentityServer.Core.Configuration.AuthenticationOptions
                {
                    IdentityProviders = ConfigureIdentityProviders
                }
            };

            //AntiForgeryConfig.UniqueClaimTypeIdentifier = Constants.ClaimTypes.Subject;
            //JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            app.UseIdentityServer(options);
        }

        public void ConfigureIdentityProviders(IAppBuilder appBuilder, string signInAsType)
        {
            appBuilder.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                AuthenticationType = "Google",
                Caption = "Sign in with Google",
                SignInAsAuthenticationType = signInAsType,
                ClientId = "101955138407-pdiru9pqq77kgrf385lsqmchjppbl1kh.apps.googleusercontent.com",
                ClientSecret = "Rb-Kv-ZQbogFWGQOljYMPTuC"
            });

            appBuilder.UseFacebookAuthentication(new FacebookAuthenticationOptions()
            {
                AuthenticationType = "Facebook",
                Caption = "Sign in with Facebook",
                SignInAsAuthenticationType = signInAsType,
                AppId = "346146348909757",
                AppSecret = "2d9b2e531246a3a3cc83955babe4a5ba",
            });
        }

        private X509Certificate2 LoadCertificate()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"idsrv3test.pfx");
            return new X509Certificate2(path, "idsrv3test");
        }


    }
}
